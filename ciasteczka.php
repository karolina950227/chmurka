<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>

						<a href="index.php">Home</a>

					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
				<div>
			<ul>
				<h1>Ciasteczka…!</h1>
				<span style="font-size: 16px;">
					Wpadnij do nas na wyśmienite ciasteczka zrobione z domowych przepisów! Przepyszne!
				</span>
				<div>
				<li style="margin-top: 25px;">
					<img src="images/1111.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Ciastka owsiane
					</span>
				</li>
				
				<li style="margin-top: 25px;">
					<img src="images/2222.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Ciastka kruche na maśle z cukrem
				</li>
				
				<li>
					<img src="images/3333.jpg" width ="250px">
					</img>
					<span style="font-size:18px; color:red; width:150px;"> 
						</br>Ciastka kruche z czarną porzeczką
					</span>
				</li>
				
				<li>
					<img src="images/4444.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Ciastka maślane z nadzieniem cappucino
					</span>
				</li>
				
				<li>
					<img src="images/5555.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Maczki z marmoladą
					</span>
				</li>
				
				<li>
					<img src="images/6666.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Rożki rumowe z alkoholem
					</span>
				</li>
				
				</div>
			</ul>
		</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>