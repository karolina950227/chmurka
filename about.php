<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title> O frimie - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
					
				</div>
				<div>
					<div>
						<a href="signup.php">Zarejestruj</a>
						<a href="index.php">Home</a>
						<a href="signin.php" class="last">Zaloguj</a>
					</div>
				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li class="current"><a href="about.php">O firmie</a></li>
				<li><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div id="about">
			<div class="aside">
				<h1>O Cukierni pod Chmurką</h1>
				<span>Opolska cukiernia utworzona w 1980 roku</span>
				<p>Cukiernia została założona przez Karolinę Dubowską, Natalię Adamek i Łukasza Bociągę. Pierwsza cukiernia powstała we wrześniu 1980 roku na jednym z opolskich osiedli i zatrudniała wtedy 2 pracowników. Był to mały sklepik z niewiele większym zapleczem produkcyjnym.</p>
				<span>Pasja</span>
				<p>Znajomość i ciągłe doskonalenie sztuki cukierniczej a także ambicje założycieli sprawiły, że firma prężnie się rozwijała i odnosiła liczne sukcesy. Dziś jest to jedno z najnowocześniejszych i największych przedsiębiorstw branży cukierniczej w Polsce.</p>
				<span>NAGRODY I WYRÓŻNIENIA</br></span>
				<p><em><strong></br>1988r.</strong> Złoty Medal na Wystawie Cukierniczej w Warszawie</em></p>
				<p><em><strong>1996r.</strong> Złoty Medal za tort w Mistrzostwach Świata Cukierników w Mediolanie</em></p> 
				<p><em><strong>1997r.</strong> Złoty Medal na Wystawie Cukierniczej IKF Stuttgart.</em></p> 
				<p><em><strong>1998r</strong>. Złoty Medal MTP ”Polagra Food” &nbsp;w Poznaniu za Tort Kandulski.</em></p> 
				<p><em><strong>1999r</strong>. Godło TERAZ POLSKA za Tort Kandulski, oraz za całokształt działalności w branży cukierniczej.</em></p> 
				<p><em><strong>1999r</strong>. Oznaka Honorowa za zasługi z Ministerstwa Oświaty,</em></p> 
				<p><em><strong>2000r.</strong> Srebrny Krzyż Zasługi za zasługi dla rzemiosła,</em></p> 
				<p><em><strong>2000r.</strong> Złoty Hipolit w konkursie „Dobre bo Polskie” za „Tort Kandulski”</em></p> 
				<p><em><strong>2000r.</strong> I miejsce i Złoty Medal na Mistrzostwach Świata Młodych Cukierników – Lizbona</em></p> 
				<p><em><strong>2001r</strong>. Złoty Medal im. Jana Kilińskiego dla Firmy za zasługi dla Rzemiosła Polskiego</em></p> 
				<p><em><strong>2002r</strong>. I miejsce i Złoty Medal na Mistrzostwach Świata Młodych Cukierników, Poznań</em></p> 
				<p><em><strong>2003r.</strong> Kryształowy Puchar Wielkopolski dla najlepszej firmy cukierniczej w Polsce</em></p> 
				<p><em><strong>2004r.</strong> Złoty Medal za „Tort Gruszkowy” – Międzynarodowe Targi Bydgoskie</em></p> 
				<p><em><strong>2005r.</strong> Złoty Medal Międzynarodowych Targów Poznańskich „Polagra Food” dla Tortu Europejskiego</em></p> 
				<p><em><strong>2006r.</strong> . I miejsce i Złoty Medal, Międzynarodowy Festiwal Rzeźbienia w Lodzie, Poznań</em></p> 
				<p><em><strong>2007r.</strong> . I miejsce i Złoty Medal, Międzynarodowy Festiwal Rzeźbienia w Lodzie, Poznań</em></p> 
				<p><em><strong>2006r.</strong> Nagroda specjalna – wyróżnienie dziennikarzy &nbsp;podczas Lodowego Pucharu Świata w Rimini</em></p> 
				<p><em><strong>2009r.</strong> I miejsce i Złoty Medal, Międzynarodowy Konkurs Artystycznego Rzeźbienia w Lodzie, Ottawa</em></p> 
				<p><em><strong>2010r.</strong> II miejsce i Srebrny Medal, Międzynarodowy Festiwal Rzeźbienia w Lodzie w Berlinie, 2010</em></p> 
				<p><em><strong>2010r.</strong> Nagroda specjalna – wyróżnienie rzeźbiarzy podczas Olimpijskich Mistrzostw Świata, Vancouver</em></p> 
				<p><em><strong>2010r.</strong> II miejsce i Srebrny Medal, Mistrzostwa Świata w Artystycznym Rzeźbieniu w Lodzie, Alaska<br /><br /><br /></em></p> 
			</div>
			<div class="section">
				<div id="visitshop">
					<div>
						<p><span>Słodycze od serca</span> Wyjątkowe wypieki</p>
						 <a href="product.php" class="visit">Zobacz ofertę</a>
					</div>
				</div>

				<div>
					<h3>Adres</h3>
					<p>ul. Prószkowksa 45 <br /> 45-777 Opole <br /> Polska </p>
				</div>
				<div>
					<a href="contact.php" class="callus">Zadzwoń <span>600 700 800</span></a>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="section">
			<div>
				<div class="aside">
					<div>
						<div>
							<b> <span>ZADZWOŃ</span> </b>
							<a href="contact.php">600 700 800</a>
							<p> <center>aby złożyć zamówienie</center></p>
						</div>
					</div>
				</div>
				<div class="connect">
					<span>Tu nas znajdziesz:</span>
					<ul>
						<li><a href="http://facebook.com/cukierniapodchmurka" target="_blank" class="facebook">Facebook</a></li>
						<li><a href="http://twitter.com/cukierniapodchmurka" target="_blank" class="twitter">Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>