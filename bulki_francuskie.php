<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>

						<a href="index.php">Home</a>

					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div>
			<ul>
				<h1>Szlachetne francuskie ciasto</h1>
				<span style="font-size: 16px;">
					Wystarczy smacznie pachnąca bułka z ciasta francuskiego ze słodkim lub słonym nadzieniem, kawa z mlekiem i odrobiną cynamonu, a poczujesz się jakbyś właśnie wpadł na przekąskę do paryskiej kafejki… O lala! Warto smakować takie wyjątkowe chwile, nawet przy biurku w pracy!
				</span>
				<div>
				<li style="margin-top: 25px;">
					<img src="images/11.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Bułka francuska z jabłkiem 
					</span>
				</li>
				
				<li style="margin-top: 25px;">
					<img src="images/22.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Bułka francuska z serem 
					</span>
				</li>
				
				<li>
					<img src="images/33.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size:18px; color:red;"> 
						</br>Bułka francuska z marmoladą 
					</span>
				</li>
				
				<li>
					<img src="images/44.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Bułka francuska z czekoladą 
					</span>
				</li>
				
				<li>
					<img src="images/55.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Bułka francuska z kapustą 
					</span>
				</li>
				
				<li>
					<img src="images/66.jpg" width ="250px" height = "150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Bułka francuska ze szpinakiem 
					</span>
				</li>
				
				</div>
			</ul>
		</div>
	</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>
