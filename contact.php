<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Kontakt - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
				    
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>
						<a href="signup.php">Zarejestruj</a>
						<a href="index.php">Home</a>
						<a href="signin.php" class="last">Zaloguj</a>
					</div>
				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li class="current"><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div>
			<h1>Napisz do nas!</h1>
			<div id="visitshop">
				<div>
					<p><span>Słodycze od serca</span> Wyjątkowe wypieki</p>
					 <a href="product.php" class="visit">Zobacz ofertę</a>
				</div>
			</div>
			<form action="#">
				<p>Jeżeli chcesz uzyskać dodatkowych informacji lub chcesz się czegoś dowiedzieć, napisz do nas!</span></p>
				<input type="text" maxlength="30" value="Imię" class="textcontact" />
				<input type="text" maxlength="30" value="E-mail" class="textcontact" />
				<input type="text" maxlength="30" value="Temat" class="textcontact" />
				<textarea name="message" id="Wiadomość" cols="30" rows="10"></textarea>
				<input type="submit" value="" class="submit" />
			</form>
		</div>
	</div>
	<div id="footer">
		<div class="section">
			<div>
				<div class="aside">
					<div>
						<div>
							<b> <span>ZADZWOŃ</span> </b>
							<a href="contact.php">600 700 800</a>
							<p> <center>aby złożyć zamówienie</center></p>
						</div>
					</div>
				</div>
				<div class="connect">
					<span>Tu nas znajdziesz:</span>
					<ul>
						<li><a href="http://facebook.com/cukierniapodchmurka" target="_blank" class="facebook">Facebook</a></li>
						<li><a href="http://twitter.com/cukierniapodchmurka" target="_blank" class="twitter">Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>