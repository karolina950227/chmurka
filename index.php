<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>
						<a href="index.php">Home</a>
						<a href="signin2.php" class="last">PRACOWNIK</a>
						<a href="signin.php" class="last">ADMINISTRATOR</a>
					</div>

				</div>
			</div>
			<ul>
				<li class="current"><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div id="section">
				<ul>
					<li><a href="p.php">PĄCZKI</a></li>
					<li><a href="bulki_drozdzowe.php">BUŁKI DROŻDŻOWE</a></li>
					<li><a href="bulki_francuskie.php">bułki francuskie</a></li>
					<li><a href="Ciasteczka.php">Ciasteczka</a></li>
					<li><a href="ciasta.php">ciasta</a></li>
					<li><a href="torty.php">torty</a></li>
				</ul>
				<a href="index.php"><img src="images/baner3.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div class="home">
			<div class="aside">
				<h1>Witamy na stronie Cukiernia pod Chmurką</h1>
				<p> Tradycyjny smak domowych wypieków pielęgnowany od 1980 roku. Każdego dnia pieczemy słodkości, które sami uwielbiamy. Pragniemy by nasze ciasta, ciasteczka i desery były naturalnym i pysznym sposobem na udane rodzinne spotkanie. Chcemy dzielić się tym co mamy najlepsze, towarzyszyć przy wspólnym stole i osładzać zarówno wyjątkowe chwile, jak i zwykłe dni.</p> 
				<p> W naszych cukierniach wypiekamy wyroby oparte o tradycyjne, regionalne receptury. Ich domowy smak znany nam doskonale z babcinych stołów, pozostaje ciągle z nami. Przywiązani do tradycji ciągle patrzymy przed siebie wzbogacając swoją słodką ofertę.</p> 
				 <a href="about.php" class="readmore">"Czytaj więcej..."</a></p>
			</div>
			<div class="section">
				<div>
					<h2>Historia</h2>
					<p>Cukiernia pod Chmurką to firma rodzinna, która powstała w 1980 roku. Wypieki oparte są na tradycyjnych śląskich recepturach. Priorytetem dla nas jest zadowolenie klienta. Dbamy, by nasza załoga regularnie uczestniczyła w szkoleniach podnoszących ich kwalifikacji, a co za tym idzie jakość i walory smakowe wyrobów cukierniczych.</p>
				</div>
				<ul>
				    
					<li class="first">
						<a href="index.php"><img src="images/cake.jpg" alt="Image" /></a>
					</li>
					<li>
						<a href="index.php"><img src="images/burgercake.jpg" alt="Image" /></a>
					</li>
					<li>
						<a href="index.php"><img src="images/cupcake.jpg" alt="Image" /></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="home">
			<div>
				<div class="aside">
					<div class="signup">
						<div>
							<b> <span>ZADZWOŃ</span> </b>
							<a href="contact.php">600 700 800</a>
							<p> <center>aby złożyć zamówienie</center></p>
						</div>
					</div>
					<div class="connect">
						<span>Tu nas znajdziesz:</span>
						<ul>
							<li><a href="http://facebook.com/cukierniapodchmurka" target="_blank" class="facebook">Facebook</a></li>
							<li><a href="http://twitter.com/cukierniapodchmurka" target="_blank" class="twitter">Twitter</a></li>
						</ul>
					</div>
				</div>
				<div class="section">
					<div>
						<div>
							<h3><a href="index.php">Co tydzień nowość!</a></h3>
							<p>Aby urozmaicić ofertę, co tydzień dodajemy do niej nowy wyrób. Wzbogacamy naszą ofartę, żeby sprostać Państwa oczekiwaniom. </p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>