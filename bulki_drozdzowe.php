<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>

						<a href="index.php">Home</a>

					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div>
				<h1>Świeżutkie drożdżówki</h1>
				<span style="font-size: 16px;">
					Wypiekane z najlepszej mąki, doskonale wyrośnięte, okrąglutkie, z przednimi dodatkami i cudowną kruszonką, smakują jak te podawane u mamy czy babci podczas niedzielnego podwieczorku. Skosztuj i zanurz się w bardzo słodkich wspomnieniach…</br>
				</span>
				
				<div style="margin-top:25px;">
					<img src="images/1.jpg" width=275px;> </img>
					<img src="images/2.jpg" width=275px;> </img>
					<img src="images/3.jpg" width=275px;> </img>
					<img src="images/4.jpg" width=275px;> </img>
					<img src="images/5.jpg" width=275px;> </img>
					<img src="images/6.jpg" width=275px;> </img>
				</div>	
		</div>
	</div>
	<div id="footer">
		<div class="section">
			<div>
				<div class="aside">
					<div>
						<div>
							<b> <span>ZADZWOŃ</span> </b>
							<a href="contact.php">600 700 800</a>
							<p> <center>aby złożyć zamówienie</center></p>
						</div>
					</div>
				</div>
				<div class="connect">
					<span>Tu nas znajdziesz:</span>
					<ul>
						<li><a href="http://facebook.com/cukierniapodchmurka" target="_blank" class="facebook">Facebook</a></li>
						<li><a href="http://twitter.com/cukierniapodchmurka" target="_blank" class="twitter">Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>