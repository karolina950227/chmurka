<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>
						<a href="signup.php">Zarejestruj</a>
						<a href="index.php">Home</a>
						<a href="signin.php" class="last">Zaloguj</a>
					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
				<div>
			<ul>
				<h1>Torty!</h1>
				<span style="font-size: 16px;">
					Przepyszne torty z tradycyjnych przepisów! Robione z najwyższej jakości produktów.
				</span>
				<div>
				<li style="margin-top: 25px;">
					<img src="images/11111.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Tort na zimno z truskawkami
					</span>
				</li>
				
				<li style="margin-top: 25px;">
					<img src="images/22222.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Tort ciasteczkowy
				</li>
				
				<li>
					<img src="images/33333.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size:18px; color:red; width:150px;" height="150px"> 
						</br>Tort brzoskwiniowy z jogurtem
					</span>
				</li>
				
				<li>
					<img src="images/44444.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Tort wiśniowo-czekoladowy
					</span>
				</li>
				
				<li>
					<img src="images/55555.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Tort porzeczkowy
					</span>
				</li>
				
				<li>
					<img src="images/66666.jpg" width ="225px" height="150px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Tort jagodowy
					</span>
				</li>
				
				</div>
			</ul>
		</div>
		</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>