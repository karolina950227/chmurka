<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>
					
						<a href="index.php">Home</a>
						
					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div>
			<ul>
				<h1>Pączkowy zawrót głowy</h1>
				<span style="font-size: 16px;">
					Pulchne, brązowe pyszotki o rozmaitych nadzieniach i kształtach, oblane słodkim lukrem lub przyprószone lekkim jak śnieg cukrem pudrem, tradycyjnie smażone na smalcu. Najlepszy deser dla koneserów słodkości i wielbicieli tłustego czwartku. Teraz możesz świętować każdego dnia!
				</span>
				<div>
				<li style="margin-top: 25px;">
					<img src="images/paczek-z-czekolada1.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Pączek z nadzieniem czekoladowym 
					</span>
				</li>
				
				<li style="margin-top: 25px;">
					<img src="images/paczek-z-kawa1.jpg" width ="250px">
					</img>
					<span style="font-size: 18px; color:red;"> 
						</br>Pączek z nadzieniem kawowym
					</span>
				</li>
				
				<li>
					<img src="images/paczek-z-kiwi1.jpg" width ="250px">
					</img>
					<span style="font-size:18px; color:red;"> 
						</br>Pączek z marmolada kiwi
					</span>
				</li>
				
				<li>
					<img src="images/paczek-z-morela1.jpg" width ="250px">
					</img>
					<span style="font-size:18px; color:red;"> 
						</br>Pączek z nadzieniem morelowym
					</span>
				</li>
				
				<li>
					<img src="images/pączek-śliwka1.jpg" width ="250px">
					</img>
					<span style="font-size:18px; color:red;"> 
						</br>Pączek z powidłami śliwkowymi
					</span>
				</li>
				
				<li>
					<img src="images/pączek-z-serem1.jpg" width ="250px">
					</img>
					<span style="font-size:18px; color:red;"> 
						</br>Pączek z nadzieniem serowym
					</span>
				</li>
				
				</div>
			</ul>
		</div>
	</div>

		<div id="navigation">
			<div>
				<p> <br />  </p>
				<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
			</div>
		</div>
	</div>
</body>
</html>