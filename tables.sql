create table Pracownik
(
   ID_Pracownika           BIGINT(11)  		NOT NULL,
   Pesel_Pacjenta          BIGINT(11)       NOT NULL,
   Nazwisko_Pracownika     VARCHAR(256)     NOT NULL,
   Imie_Pracownika         VARCHAR(256)     NOT NULL,
   Adres_Pracownika	       VARCHAR(256)     NOT NULL,
   Login_Pracownika        VARCHAR(256)     NOT NULL,
   Haslo_Pracownika        VARCHAR(20)      NOT NULL,
   Telefon_Pracownika      BIGINT(9)        NOT NULL,
   Rola		               BOOL     		NOT NULL,   
   constraint PK_PRACOWNIK primary key (ID_Pracownika)
);





create table Oferta
(
   ID_Oferty           BIGINT(11)  NOT NULL,
   Rodzaj_Oferty       VARCHAR(256)   NOT NULL,
   constraint PK_OFERTA primary key (ID_Oferty)
);


create table Oferta_Cukiernia
(
   Oferta           BIGINT(11)  NOT NULL,
   constraint PK_OFERTA_CUKIERNIA primary key (Oferta)
);

create table Cukiernia
(
   Oferta_Cukierni           BIGINT(11)  NOT NULL,
   Adres	                 VARCHAR(256)      NOT NULL,
   Pracownik		         BIGINT(11)        NOT NULL,
   constraint PK_CUKIERNIA primary key (Oferta_Cukierni)
);

create table Wyrob
(
   ID_Wyrobu          BIGINT(11)  NOT NULL,
   Cena		          FLOAT(5,2)    NOT NULL,   
   Jednostka_Miary    VARCHAR(256)   NOT NULL,
   constraint PK_WYROB primary key (ID_Wyrobu)
);


create table Media
(
   ID_Zdjecia 		INt NOT NULL AUTO_INCREMENT,
   Opis 			VARCHAR(40),
   Zdjecie          LONGBLOB,
   constraint PK_MEDIA primary key (ID_Zdjecia)
);