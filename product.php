<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Oferta - Cukiernia pod Chmurka</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="css/ie8.css" />
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="css/ie7.css" />
	<![endif]-->
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<div>
				<div id="logo">
					<a href="index.php"><img src="images/logo.png" alt="Logo"/></a>
				</div>
				<div>
					<div>
						<a href="signup.php">Zarejestruj</a>
						<a href="index.php">Home</a>
						<a href="signin.php" class="last">Zaloguj</a>
					</div>

				</div>
			</div>
			<ul>
				<li><a href="index.php">Strona główna</a></li>
				<li><a href="about.php">O firmie</a></li>
				<li class="current"><a href="product.php">Oferta</a></li>				
				<li><a href="services.php">Gdzie kupić</a></li>
				<li><a href="contact.php">Kontakt</a></li>
			</ul>
			<div class="section">
				<a href="index.php"><img src="images/baner4.jpg" alt="Image"/></a>
			</div>
		</div>
	</div>
	<div id="content">
		<div>
			<h1>Oferta</h1>
			<ul>
				<li>
					<div>
						<div>
							<h2><a href="p.php">Pączki</a></h2>
							<a href="p.php" class="view">zobacz</a>
						</div>
						<a href="p.php"><img src="images/special-treats.jpg" alt="Image" /></a>
					</div>
				</li>
				<li>
					<div>
						<div>
							<h2><a href="bulki_drozdzowe.php">Bułki drożdżowe</a></h2>
							<a href="bulki_drozdzowe.php" class="view">zobacz</a>
						</div>
						<a href="bulki_drozdzowe.php"><img src="images/tarts.jpg" alt="Image" /></a>
					</div>
				</li>
				<li>
					<div>
						<div>
							<h2><a href="bulki_francuskie.php">Bułki francuskie</a></h2>
							<a href="bulki_francuskie.php" class="view">zobacz</a>
						</div>
						<a href="bulki_francuskie.php"><img src="images/cakes.jpg" alt="Image" /></a>
					</div>
				</li>
				<li>
					<div>
						<div>
							<h2><a href="ciasteczka.php">Ciasteczka</a></h2>
							<a href="ciasteczka.php" class="view">zobacz</a>
						</div>
						<a href="ciasteczka.php"><img src="images/dessert.jpg" alt="Image" /></a>
					</div>
				</li>
				<li>
					<div>
						<div>
							<h2><a href="ciasta.php">Ciasta</a></h2>
							<a href="ciasta.php" class="view">zobacz</a>
						</div>
						<a href="ciasta.php"><img src="images/pastries.jpg" alt="Image" /></a>
					</div>
				</li>
				<li>
					<div>
						<div>
							<h2><a href="torty.php">Torty</a></h2>
							<a href="torty.php" class="view">zobacz</a>
						</div>
						<a href="torty.php"><img src="images/healthy-food.jpg" alt="Image" /></a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<div class="section">
			<div>
				<div class="aside">
					<div>
						<div>
							<b> <span>ZADZWOŃ</span> </b>
							<a href="contact.php">600 700 800</a>
							<p> <center>aby złożyć zamówienie</center></p>
						</div>
					</div>
				</div>
				<div class="connect">
					<span>Tu nas znajdziesz:</span>
					<ul>
						<li><a href="http://facebook.com/cukierniapodchmurka" target="_blank" class="facebook">Facebook</a></li>
						<li><a href="http://twitter.com/cukierniapodchmurka" target="_blank" class="twitter">Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>
    		
    	<div id="navigation">
    		<div>
    			<p> <br />  </p>
    			<p>Copyright &copy; 2018 Cukiernia pod Chmurką  All rights reserved</p>
    		</div>
    	</div>
    </div>
</body>
</html>